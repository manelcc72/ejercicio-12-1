package es.manelcc.t12ej1;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {

    public static final String NEW_DATA = "com.imaginagroup.MyService.NEW_DATA";
    private static final String stringUrl = "http://www.imaginaformacion.com/recursos/rand.php";
    private static final int updateInterval = 1000;
    private Timer updateTimer;
    private TimerTask doRefresh;
    private String data;


    public MyService() {
    }

    private IBinder binder = new MyBinder();

    public class MyBinder extends Binder {
        MyService getService() {
            return MyService.this;
        }
    }

    private void updateData() {
        // Nos conectamos al servicio web y obtenemos los datos
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            int responseCode = httpConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream in = httpConnection.getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                data = reader.readLine();
                in.close();
                // Lanzamos el broadcast con el intent y los datos
                announceData();
            }
        } catch (MalformedURLException e) {
            Log.e("Service", e.getMessage());
        } catch (IOException e) {
            Log.e("Service", e.getMessage());
        }
    }

    private void announceData() {
        // Lanzamos el broadcast con el intent y los datos
        Intent intent = new Intent(NEW_DATA);
        intent.putExtra("data", data);
        sendBroadcast(intent);
    }

    public String getData() {
        return data;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Creamos el Timer y el TimerTask
        updateTimer = new Timer();
        doRefresh = new TimerTask() {
            @Override
            public void run() {
                updateData();
            }
        };
        // Lo programamos para que se lance cada updateInterval milisegundos
        updateTimer.scheduleAtFixedRate(doRefresh, 0, updateInterval);
        // Queremos que el servicio esté siempre encendido
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("SERVICIO", "ENTRO EN DESTROY SERVICE");
        updateTimer.cancel();
        doRefresh.cancel();
    }
}
