package es.manelcc.t12ej1;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView manual, automatico, estadoServicio;
    Button start, refresh, stop;
    private ReceptorServicio myReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manual = (TextView) findViewById(R.id.txtManual);
        automatico = (TextView) findViewById(R.id.txtAutomatico);
        estadoServicio = (TextView) findViewById(R.id.txtEstadoServicio);

        start = (Button) findViewById(R.id.btnStar);
        stop = (Button) findViewById(R.id.btnStop);
        refresh = (Button) findViewById(R.id.btnRefresh);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startService(
                        new Intent(MainActivity.this, MyService.class));
            }
        });

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manual.setText(myService.getData());
            }
        });


        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myService.onDestroy();
                stopService(new Intent(MainActivity.this, MyService.class));

                estadoServicio.setText("Servicio Parado...");
            }
        });

        bindService(new Intent(MainActivity.this, MyService.class),
                myConnection,
                Context.BIND_AUTO_CREATE);

        // Ejecucion remota de metodos de services

    }

    public MyService myService;
    private ServiceConnection myConnection = new ServiceConnection() {


        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            myService = ((MyService.MyBinder) arg1).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            myService = null;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ReceptorServicio extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            automatico.setText(arg1.getStringExtra("data"));
            estadoServicio.setText("Recibiendo...");
        }
    }

    @Override
    public void onResume() {
        IntentFilter filter;
        filter = new IntentFilter(MyService.NEW_DATA);
        myReceiver = new ReceptorServicio();
        registerReceiver(myReceiver, filter);
        super.onResume();
    }

    @Override
    public void onPause() {
        unregisterReceiver(myReceiver);
        super.onPause();
    }
}
